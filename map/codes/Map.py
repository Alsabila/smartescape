from Nodes import Nodes
import os

class Map:
    WIDTH = 20
    LENGTH = 20

    def __init__(self):
        self.mapped = [[0] * Map.WIDTH for n in range(Map.LENGTH)]  # bikin array of array kosong

        i = 0
        with open(os.path.join('denah', "map BS HQ.txt")) as mapfile: # ini buat liat file yg dibaca, ([folder],[file])

            # cek koordinat tujuan
            for line in mapfile:
                lineSplitX = list(line) # split per line, biar bisa baca substring
                for j in range(Map.WIDTH):
                    if lineSplitX[j] == "X":
                        self.mapped[i][j] = Nodes(i , j, 'X', i, j)  # coklat goal
                        goalx = i
                        goaly = j
                i += 1

            i = 0  # reset i

            # cek node2 lain
            mapfile.seek(0)  # pindahin pointer ke awal lagi
            for line in mapfile:
                lineSplit = list(line) # split per line, biar bisa baca substring
                for j in range(Map.WIDTH):
                    if lineSplit[j] == ".":
                        self.mapped[i][j] = Nodes(i , j, '.', goalx, goaly)  # hijau rumput
                    elif lineSplit[j] == "B":
                        self.mapped[i][j] = Nodes(i , j, 'B', goalx, goaly)  # hitam dinding
                    elif lineSplit[j] == "T":
                        self.mapped[i][j] = Nodes(i , j, 'T', goalx, goaly)  # abu meja
                    # elif lineSplit[j] == "X":
                    #     self.mapped[i][j] = Nodes(i , j, 'X', goalx, goaly)  # coklat goal
                    elif lineSplit[j] == " ":
                        self.mapped[i][j] = Nodes(i , j, ' ', goalx, goaly)  # putih jalan

                i += 1
        mapfile.close()

        # INI KALO MAU LIAT HASIL

        # print(self.getAllCoordinate())
        # print(self.printMap())
        # node = self.getNode(2,2)
        # print(node.getX(), node.getY(), node.getIsi())

    def getAllNodes(self): # KALO BUAT AMBIL MACEM2, PAKE INI AJA TRUS getIsi, getX, DLL
        return self.mapped

    def getAllIsi(self):
        outList = [[0] * len(self.mapped) for i in range(len(self.mapped))]

        for i in range(len(self.mapped)):
            for j in range(len(self.mapped[i])):
                outList[i][j] = self.mapped[i][j].getIsi()

        return outList

    def printMap(self):
        # for row in map.mapped:
        #     print(' '.join([str(elem) for elem in row]))

        # for row in map.mapped:
        #    print(' '.join([str(str(elem.getX()) + "," + str(elem.getY()) ) for elem in row]))

        out = ""
        for row in self.mapped:
            out += (' '.join([str(elem.getIsi()) for elem in row]))
            out += "\n"
        return out

    def getAllCoordinate(self):
        # out = []
        # for row in self.mapped:
        #     out += (' '.join([str(elem.getX()) + "," + str(elem.getY()) for elem in row]))
        #     out += "\n"
        # return out

        # yang di bawah salah krn bentuk array of array beda dgn mapped??????? tp kyknya bener...
        outList = [[0] * len(self.mapped) for i in range(len(self.mapped))]
        for i in range(len(self.mapped)):
            for j in range(len(self.mapped[i])):
                outList[i][j] = [self.mapped[i][j].getX(), self.mapped[i][j].getY()]

        return outList

        #ini buat cek
        # return outList[0][18] #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    def getNode(self, x, y):  # ini cm ambil node, bkn x y isi dll
        return self.mapped[x][y]

