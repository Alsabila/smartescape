from tkinter import *
from Map import *
from Nodes import *
from CreateToolTip import *

class mainGUI:
    def __init__(self):
        root = Tk()
        root.title("SmartEscape")
        root.iconbitmap('smartescape_icon.ico')
        self.inputframe = Frame()
        self.inputframe.pack( side = RIGHT )
        self.outputframe = Frame(root)
        self.outputframe.pack( side = LEFT )

        self.map = Map()
        self.createUI()
        self.createMap()

        root.mainloop()

    def createUI(self):
        inputlabelX = Label(self.inputframe, text="Enter Coordinate x")
        inputlabelX.pack()
        self.inputX = Entry(self.inputframe, bd=5)
        self.inputX.pack()
        inputlabelY = Label(self.inputframe, text="Enter Coordinate y")
        inputlabelY.pack()
        self.inputY = Entry(self.inputframe, bd=5)
        self.inputY.pack()

        enterButton = Button(self.inputframe, text="FIND ROUTE", bg='red',
        command=self.doSearch)
        enterButton.pack()

        enterButton = Button(self.inputframe, text="ULANG", bg='blue',
        command=self.doUlang)
        enterButton.pack()

        self.keterangan = Text(self.inputframe,width = 20, height = 10)
        self.keterangan.pack()
        legend = "Keterangan:\n"
        legend += "-----> y\n"
        legend += "|\n"
        legend += "|\n"
        legend += "v\n"
        legend += "x\n"
        legend += "\nHasil akan muncul di bawah ini\n"
        self.keterangan.insert(INSERT,legend)

        self.hasil = Text(self.inputframe, width = 20, height = 10)
        self.hasil.pack()

    def createMap(self):
        listMap = self.map.getAllIsi()
        listCoordinate = self.map.getAllCoordinate()
        self.listButton = [[0] * Map.WIDTH for n in range(Map.LENGTH)]
        x = y = 0

        for row in listMap:
            for column in row:
                if column == '.':
                    tooltip_teks = "Koordinat [x, y]: " + str(listCoordinate[y][x])
                    button = Button(self.outputframe, bg='green', width = 2, height = 1)
                elif column == 'B':
                    button = Button(self.outputframe, bg='black', width = 2, height = 1, state=DISABLED)
                    tooltip_teks = "WALL!"
                elif column == 'T':
                    button = Button(self.outputframe, bg='gray', width = 2, height = 1)
                    tooltip_teks = "Koordinat [x, y]: " + str(listCoordinate[y][x])
                elif column == ' ':
                    tooltip_teks = "Koordinat [x, y]: " + str(listCoordinate[y][x])
                    button = Button(self.outputframe, bg='white', width = 2, height = 1)
                elif column == 'X':
                    button = Button(self.outputframe, bg='brown', width = 2, height = 1, state=DISABLED)
                    tooltip_teks = "GO HERE!"
                button.grid(row=y,column=x)
                button_tooltip = CreateToolTip(button, tooltip_teks)
                self.listButton[y][x] = button     
                x = x + 1
            x = 0
            y = y + 1

    def doSearch(self):
        x = int(self.inputX.get())
        y = int(self.inputY.get())
        startNode = self.map.getNode(x,y)
        goalNode = self.map.getNode(1,19)
        self.listPathNode = []
        self.listVisited = []
        self.BestFirstSearch(startNode,goalNode)

    def BestFirstSearch(self, start, goal):
        return self.BFS(start,goal)

    def BFS(self, start, goal):
        self.listPathNode.append(start)
        self.listVisited.append(start)

        while start.getHeur() != 0:
            self.listButton[start.getX()][start.getY()].configure(bg='yellow')
            successors = []

            if start.getX() != 19 :
                down_suc = self.map.getNode(start.getX()+1,start.getY())
                if down_suc.getIsi() != 'B' and down_suc.getIsi() != 'T':
                    if down_suc not in self.listVisited:
                        successors.append(down_suc)

            if start.getY() != 19 :
                right_suc = self.map.getNode(start.getX(),start.getY()+1)
                if right_suc.getIsi() != 'B' and right_suc.getIsi() != 'T':
                    if right_suc not in self.listVisited:
                        successors.append(right_suc)

            if start.getY() != 0 :
                left_suc = self.map.getNode(start.getX(),start.getY()-1)
                if left_suc.getIsi() != 'B' and left_suc.getIsi() != 'T':
                    if left_suc not in self.listVisited:
                        successors.append(left_suc)
                    
            if start.getX() != 0 :
                up_suc = self.map.getNode(start.getX()-1,start.getY())
                if up_suc.getIsi() != 'B' and up_suc.getIsi() != 'T':
                    if up_suc not in self.listVisited:
                        if up_suc.getY() != 3:
                            successors.append(up_suc)
                        elif up_suc.getX() == 16 and up_suc.getY() == 3:
                            successors.append(up_suc)

            def sorting(node):
                return node.getHeur()
            
            successors.sort(key=sorting)
            start = successors[0]
            self.listPathNode.append(start)
            self.listVisited.append(start)
        
        self.text_hasil = "Jarak terdekat: " + str(len(self.listPathNode)-1) + "\n \n"
        self.text_hasil += "- Jangan panik\n"
        self.text_hasil += "- Jangan lari\n" 
        self.text_hasil += "- Jangan balik lagi\n" 
        self.text_hasil += "- Jangan lupa berdoa\n\n"
        self.text_hasil += 'from BroSisCerdas :)'
        self.hasil.insert(INSERT, self.text_hasil)
    
    def doUlang(self):
        self.outputframe.grid_forget()
        self.hasil.delete(1.0,END)
        self.createMap()

mainGUI()
