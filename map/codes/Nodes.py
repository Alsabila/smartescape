import math

class Nodes:
    def __init__(self, x, y, isi, goalx, goaly):
        self.x = x
        self.y = y
        self.isi = isi
        self.startx = 0  # default sebelum start dipilih
        self.starty = 0  # default sebelum start dipilih
        self.distToStart = math.inf  # default sebelum start dipilih
        self.goalx = goalx
        self.goaly = goaly
        self.heur = self.setHeur(isi)
        self.fSCore = math.inf  # default sebelum start dipilih

        # startx, starty, distToStart, fScore ga diitung dulu sebelum start nya kepilih biar nilainya ga infinite, tapi heur bisa diitung karena goal udah ketauan

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def getIsi(self):
        return self.isi

    def getHeur(self):
        return self.heur

    def getFScore(self):
        return self.fSCore

    def setStart(self,x,y):
        self.startx = x
        self.starty = y

    def getGoalX(self):
        return self.goalx
    
    def getGoalY(self):
        return self.goaly

    def setHeur(self, isi):
        # cek kalo isinya adalah dinding / meja, tidak bisa dilewati sehingga heuristiknya infinite
        if isi != "B" and isi != "T":
            out = abs(self.x - self.goalx) + abs(self.y - self.goaly)
        elif isi == 'X':
            out = 0
        else:
            out = math.inf
        return out

    def setDistToStart(self):
        if self.startx != self.x and self.starty != self.y:  # kalo bukan start
            out = abs(self.x - self.startx)  + abs(self.y - self.starty)
        else:
            out = 0
        self.distToStart =  out

    def setFScore(self):
        if self.startx != self.x and self.starty != self.y:  # kalo bukan start
            self.fSCore = self.heur + self.distToStart
        else:
            self.fSCore = self.heur
    # Nanti, cek bisa jalan ke node lain / tidaknya pake cek isi (bukan cek tipe node)