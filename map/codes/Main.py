from Map import Map

class main:
    peta = Map()

    # TEST
    print(peta.printMap())
    print(peta.getAllCoordinate())
    print(peta.getAllNodes())
    print(peta.getAllIsi())

    node = peta.getNode(2,3)
    print(node.getX(), node.getY(), node.getIsi(), node.getHeur())

    node = peta.getNode(2,2)
    print(node.getX(), node.getY(), node.getIsi(), node.getHeur())

    node = peta.getNode(2,1)
    print(node.getX(), node.getY(), node.getIsi(), node.getHeur())